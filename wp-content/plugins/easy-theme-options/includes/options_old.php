<?php

/* ----------------------------------------
* To retrieve a value use: $eto_options[$prefix.'var']
----------------------------------------- */

$prefix = 'eto_';

/* ----------------------------------------
* Create the TABS
----------------------------------------- */

$eto_custom_tabs = array(
		array(
			'label'=> __('Settings', 'eto'),
			'id'	=> $prefix.'settings'
		)/*,
		array(
			'label'=> __('Opciones Avanzadas 1', 'eto'),
			'id'	=> $prefix.'avanzadas'
		),
		array(
			'label'=> __('Opciones Avanzadas 2', 'eto'),
			'id'	=> $prefix.'avanzadas2'
		)*/
	);

/* ----------------------------------------
* Options Field Array
----------------------------------------- */

$eto_custom_meta_fields = array(

	/* -- TAB 1 -- */
	array(
		'id'	=> $prefix.'settings', // Use data in $eto_custom_tabs
		'type'	=> 'tab_start'
	),
	
	array(
		'label'=> 'OFFICIAL DISTRIBUTOR MANAGEMENT',
		'id'	=> $prefix.'titledistributor',
		'type'	=> 'title'
	),

	array(
		'label'=> 'Official distributor text',
		'desc'	=> 'Text for the official distributor.',
		'id'	=> $prefix.'distributor-text',
		'type'	=> 'text'
	),

	array(
		'label'=> 'Official distributor text link',
		'desc'	=> 'Link for the official distributor text.',
		'id'	=> $prefix.'distributor-text-link',
		'type'	=> 'text'
	),

	array(
		'label'=> 'Official distributor image',
		'desc'	=> 'Image for the official distributor.',
		'id'	=> $prefix.'distributor-image',
		'type'	=> 'image'
	),

	array(
		'label'=> 'Official distributor image link',
		'desc'	=> 'Link for the official distributor image.',
		'id'	=> $prefix.'distributor-image-link',
		'type'	=> 'text'
	),

	array(
		'label'=> 'BUY THERMOMIX BUTTON MANAGEMENT',
		'id'	=> $prefix.'titlebuythermomixbutton',
		'type'	=> 'title'
	),

	array(
		'label'=> 'Buy Thermomix button label',
		'desc'	=> 'The label of the button.',
		'id'	=> $prefix.'buy-thermomix-label',
		'type'	=> 'text'
	),

	array(
		'label'=> 'Buy Thermomix button destination',
		'desc'	=> 'The url to buy page. (Absolute path recommended)',
		'id'	=> $prefix.'buy-thermomix-url',
		'type'	=> 'text'
	),

	array(
		'label'=> 'BUY THERMOMIX PAGE MANAGEMENT',
		'id'	=> $prefix.'titlebuythermomixpage',
		'type'	=> 'title'
	),

	array(
		'label'=> 'Code for Contact Form',
		'desc'	=> 'The Code to generate de Contact Form',
		'id'	=> $prefix.'buy-thermomix-code',
		'type'	=> 'text'
	),

	array(
		'label'=> 'REPRÄSENTANTIN WERDEN PAGE MANAGEMENT',
		'id'	=> $prefix.'titlereppage',
		'type'	=> 'title'
	),

	array(
		'label'=> 'Code for Contact Form',
		'desc'	=> 'The Code to generate de Contact Form',
		'id'	=> $prefix.'rep-code',
		'type'	=> 'text'
	),

	array(
		'label'=> 'SLIDER',
		'id'	=> $prefix.'sliderimages',
		'type'	=> 'title'
	),

	array(
		'label'=> 'Video #1',
		'id'	=> $prefix.'video1',
		'type'	=> 'title'
	),

	array(
		'label'	=> 'Video #1',
		'desc'	=> 'Url of the video #1',
		'id'	=> $prefix.'video_url1',
		'type'	=> 'text'
	),

	array(
		'label'=> 'Video #1 thumb image',
		'desc'	=> 'Thumb image for video #1 (960px x 398px)',
		'id'	=> $prefix.'video_thumb1',
		'type'	=> 'image'
	),

	array(
		'label'=> 'Video #2',
		'id'	=> $prefix.'video2',
		'type'	=> 'title'
	),

	array(
		'label'	=> 'Video #2',
		'desc'	=> 'Url of the video #2',
		'id'	=> $prefix.'video_url2',
		'type'	=> 'text'
	),

	array(
		'label'=> 'Video #2 thumb image',
		'desc'	=> 'Thumb image for video #2 (960px x 398px)',
		'id'	=> $prefix.'video_thumb2',
		'type'	=> 'image'
	),
	
	array(
		'label'=> 'Video #3',
		'id'	=> $prefix.'video3',
		'type'	=> 'title'
	),

	array(
		'label'	=> 'Video #3',
		'desc'	=> 'Url of the video #3',
		'id'	=> $prefix.'video_url3',
		'type'	=> 'text'
	),

	array(
		'label'=> 'Video #3 thumb image',
		'desc'	=> 'Thumb image for video #3 (960px x 398px)',
		'id'	=> $prefix.'video_thumb3',
		'type'	=> 'image'
	),
	
	array(
		'label'=> 'Video #4',
		'id'	=> $prefix.'video4',
		'type'	=> 'title'
	),

	array(
		'label'	=> 'Video #4',
		'desc'	=> 'Url of the video #4',
		'id'	=> $prefix.'video_url4',
		'type'	=> 'text'
	),

	array(
		'label'=> 'Video #4 thumb image',
		'desc'	=> 'Thumb image for video #4 (960px x 398px)',
		'id'	=> $prefix.'video_thumb4',
		'type'	=> 'image'
	),

	array(
		'label'	=> 'Footer description and images management',
		'id'	=> $prefix.'footermanagement',
		'type'	=> 'title'
	),

	array(
		'label'=> 'Vorwerk history description',
		'desc'	=> 'The history of Vorwerk.',
		'id'	=> $prefix.'vorwerk-history',
		'type'	=> 'textarea'
	),

	array(
		'label'	=> 'Vorwerk products image',
		'desc'	=> 'Image of Vorwerk products.',
		'id'	=> $prefix.'vorwerk-products',
		'type'	=> 'image'
	),

	array(
		'label'	=> 'Social Networks',
		'id'	=> $prefix.'socialnetworks',
		'type'	=> 'title'
	),

	array(
		'label'=> 'Follow us on:',
		'desc'	=> 'If you want to change this text.',
		'id'	=> $prefix.'follow-us',
		'type'	=> 'text'
	),

	array(
		'label'=> 'Facebook:',
		'desc'	=> 'Url to the Facebook fanpage.',
		'id'	=> $prefix.'fb',
		'type'	=> 'text'
	),

	array(
		'label'=> 'LinkedIn:',
		'desc'	=> 'Url to the LinkedIn profile page.',
		'id'	=> $prefix.'in',
		'type'	=> 'text'
	),

	array(
		'label'=> 'YouTube:',
		'desc'	=> 'Url to the YouTube channel page.',
		'id'	=> $prefix.'yt',
		'type'	=> 'text'
	),

	array(
		'type'	=> 'tab_end'
	),

	/* -- TAB 1 -- */

	
);

?>
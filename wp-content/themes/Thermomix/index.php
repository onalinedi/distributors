<?php
get_header();
?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jwplayer/jwplayer.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/aslider/js/swfobject.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/aslider/css/anythingslider.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/aslider/js/jquery.anythingslider.js"></script>    
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/aslider/js/jquery.anythingslider.video.js"></script>

<div id="wrapper-index">
    <?php
    // Small menu
    ?>			
    <div id="wrapper-searcherheader">
        <div id="right-header">
            <div id="wrapper-search">
                <?php include (TEMPLATEPATH . '/searchform.php'); ?>
            </div>
            <nav id="wrapper-contact">
                <?php
                $defaults = array(
                    'theme_location' => 'top-menu',
                    'menu' => '',
                    'container' => 'div',
                    'container_class' => '',
                    'container_id' => '',
                    'menu_class' => 'menu',
                    'menu_id' => '',
                    'echo' => true,
                    'fallback_cb' => 'wp_page_menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth' => 0,
                    'walker' => ''
                );
                wp_nav_menu($defaults);
                ?>
            </nav>
            <div class="clear"></div>
        </div>
    </div>	<!-- wrapper -->	
    <?
    // end Small menu
    ?>
    <div id="wrapper-slider">
        <ul id="slider">
            <?php
            // Video #1
            if (eto_get_option('eto_video_url1') <> '') {
                ?>
                <li><div id="container1"></div></li>
                <?php
            }
            ?>
            <?php
            // Video #2
            if (eto_get_option('eto_video_url2') <> '') {
                ?>
                <li><div id="container2"></div></li>
                <?php
            }
            ?>
            <?php
            // Video #3
            if (eto_get_option('eto_video_url3') <> '') {
                ?>
                <li><div id="container3"></div></li>
                <?php
            }
            ?>
            <?php
            // Video #4
            if (eto_get_option('eto_video_url4') <> '') {
                ?>
                <li><div id="container4"></div></li>
                <?php
            }
            ?>
        </ul>

        <script type="text/javascript">
            // Make a list of videos first
            // ***********************
            var flashplayer = "<?php echo get_template_directory_uri(); ?>/jwplayer/player.swf",
                    videos = [];

<?php
// Video #1
if (eto_get_option('eto_video_url1') <> '') {
    ?>
                videos.push([
                        "container1",
                        "<?php echo eto_get_option('eto_video_url1'); ?>",
    <?php
    if (eto_get_image('eto_video_thumb1') <> '') {
        ?>
                    "<?php echo eto_get_image('eto_video_thumb1'); ?>",
        <?php
    }
    ?>
                398,
                        960
                ]);
    <?php
}
?>
<?php
// Video #2
if (eto_get_option('eto_video_url2') <> '') {
    ?>
                videos.push([
                        "container2",
                        "<?php echo eto_get_option('eto_video_url2'); ?>",
    <?php
    if (eto_get_image('eto_video_thumb2') <> '') {
        ?>
                    "<?php echo eto_get_image('eto_video_thumb2'); ?>",
        <?php
    }
    ?>
                398,
                        960
                ]);
    <?php
}
?>
<?php
// Video #3
if (eto_get_option('eto_video_url3') <> '') {
    ?>
                videos.push([
                        "container3",
                        "<?php echo eto_get_option('eto_video_url3'); ?>",
    <?php
    if (eto_get_image('eto_video_thumb3') <> '') {
        ?>
                    "<?php echo eto_get_image('eto_video_thumb3'); ?>",
        <?php
    }
    ?>
                398,
                        960
                ]);
    <?php
}
?>
<?php
// Video #4
if (eto_get_option('eto_video_url4') <> '') {
    ?>
                videos.push([
                        "container4",
                        "<?php echo eto_get_option('eto_video_url4'); ?>",
    <?php
    if (eto_get_image('eto_video_thumb4') <> '') {
        ?>
                    "<?php echo eto_get_image('eto_video_thumb4'); ?>",
        <?php
    }
    ?>
                398,
                        960
                ]);
    <?php
}
?>

            // Set up players
            $.each(videos, function(i, v) {
                jwplayer(v[0]).setup({
                    file: v[1],
                    flashplayer: flashplayer,
                    image: v[2],
                    height: v[3],
                    width: v[4]
                });
            });

            var playvid = function(slider) {
                if (jwplayer) {
                    $.each(videos, function(i) {

                        jwplayer(i).play();

                    });
                }

            }


            // Set up AnythingSlider
            $('#slider').anythingSlider({
                hashTags: false,
                // Autoplay video in initial panel, if one exists

                onInitialized: function(e, slider) {

                    playvid(slider);

                },
                // pause all videos when changing slides
                onSlideInit: function(e, slider) {
                    if (jwplayer) {
                        $.each(videos, function(i) {

                            jwplayer(i).play();
                            jwplayer(i).pause(true);
                        });
                    }
                },
                // Only play a paused video when a slide comes into view
                onSlideComplete: function(slider) {
                    if (jwplayer) {
                        $.each(videos, function(i, v) {
                            // find ID in panel - if there are two videos in the same panel, both will start playing!
                            if (slider.$currentPage.find('#' + v[0]).length && jwplayer(v[0]).getState() === 'PAUSED') {
                                jwplayer(v[0]).play();
                            }
                        });
                    }
                },
                // *********** Video ***********
                // this DOES NOTHING because JW player is set up outside of AnythingSlider, use SWFObject if you need to
                addWmodeToObject: "opaque", // deprecated option anyway ;)
                // return true if video is playing or false if not
                isVideoPlaying: function(slider) {
                    if (jwplayer) {
                        // jwplayer object is wrapped in #{id}_wrapper
                        var vid = slider.$currentPage.find('[id$=_wrapper]'),
                                jwid = (vid.length) ? vid.attr('id').replace(/_wrapper/, '') : '';
                        if (vid.find('#' + jwid).length && jwplayer(jwid).getState() === 'PLAYING') {
                            return true;
                        }
                    }
                    return false;
                }
            });
        </script>


    </div>

    <?php
    // Banner
    if (eto_get_option('eto_banner-image') <> '') {
        ?>
        <div id="wrapper-banner">
            <a href="<?php echo eto_get_option('eto_banner-image-link'); ?>"><img src="<?php echo eto_get_image('eto_banner-image'); ?>"></a>
        </div>
        <?php
    }
    ?>


    <div id="wrapper-home-block">

        <?php query_posts('post_type=home-block'); ?>
        <?php $newline = 0; ?>
        <?php while (have_posts()) : the_post(); ?>	
            <?php
            if ($newline == 2) {
                $newline = 0;
                $hbclass = "hb-right";
            } else {
                $newline++;
                $hbclass = "hb-left";
            }
            ?>
            <div class="home-block <?php echo $hbclass; ?>">
                <?php
                if (get_field('destination_type') == 'url') {
                    $urlblock = get_field('url');
                    $urltype = "url";
                } else {
                    $urlpost = get_field('content_page');
                    $urlblock = get_permalink($urlpost->ID);
                    $urltype = "cp";
                }
                ?>
                <a href="<?php echo $urlblock; ?>"><h2><?php the_title(); ?></h2></a>			
                <div class="hb-img-container" data-url="<?php echo $urlblock; ?>" data-type="<?php echo $urltype; ?>">
                    <img src="<?php echo get_field('image'); ?>" alt="">
                    <div class="slider-up">					
                        <div style="display:none;"><?php the_content(); ?></div>
                    </div>
                </div>			
            </div>
            <?php
            if ($newline == 3) {
                ?>
                <div class="clear"></div>
                <?php
            }
            ?>		
        <?php endwhile; ?>

        <div class="clear"></div>
    </div>	
</div>

<?php
get_footer();
?>
	

<?php 
	if ( is_active_sidebar( 'left-sidebar' ) ) :
		?>
	<aside>
	<?php
		dynamic_sidebar( 'left-sidebar' );
	?>
	</aside>
	<?php
	endif;
?>
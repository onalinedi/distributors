<?php
  get_header(); 
?>

<div id="wrapper-single">
	<?php if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>
	<?php 
	// Small menu
	?>
			<div id="right-header">
				<div id="wrapper-search">
					<?php include (TEMPLATEPATH . '/searchform.php'); ?>
				</div>
				<nav id="wrapper-contact">
					<?php
						$defaults = array(
							'theme_location'  => 'top-menu',
							'menu'            => '',
							'container'       => 'div',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'menu',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 0,
							'walker'          => ''
						);
						wp_nav_menu( $defaults );
					?>
				</nav>
				<div class="clear"></div>
			</div>
	<?
	// end Small menu
	?>
	<?php get_sidebar('left'); ?>
	<?php 
		if (is_active_sidebar( 'left-sidebar' )) {
			$articleclass = "sidebar-yes";
		} else {
			$articleclass = "sidebar-no";
		}
	?>
	
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<article class="<?php echo $articleclass; ?>">
		<h1 class="article-title"><?php the_title(); ?></h1>		
		<div class="content">
			<?php the_excerpt(); ?>
			<div class="clear"></div>
			<div>
				<p><a href="<?php the_permalink(); ?>">Read more »</a></p>
			</div>
		</div>		
		<div class="clear"></div>
		</article>
		<?php endwhile; // end of the loop. ?>
		<?php else : ?>

        <article>
		<h1 class="article-title"><span>Results not found</span></h1>
		<div class="content">
			There are no results that match with your criteria
		</div>
		<div class="content-image">
			
		</div>
		<div class="clear"></div>
		</article>

        <?php endif; ?>

	<div class="clear"></div>
</div>

<?php 
	get_footer(); 
?>
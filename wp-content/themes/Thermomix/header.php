<!DOCTYPE HTML>
<html lang="en-US">
<head>
<link rel=�shortcut icon" href=">?php echo get_stylesheet_directory_uri(); ?</favicon.ico� />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="<?php bloginfo( 'charset' ); ?>" />	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php echo str_replace(']','',str_replace('[','',wp_title("|",false,'right'))); ?></title>

	<!-- Le styles -->  	
  	<link href="<?php echo get_template_directory_uri(); ?>/css/normalize.css" rel="stylesheet">
  	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
  	<link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">

  	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
  	<!--[if lt IE 9]>
    	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
  	<![endif]-->
  	<!-- ***** -->
  	<?php
    	wp_head();
  	?>
<!-- ***** -->
  	<style type="text/css" media="screen">
    	html { margin-top: 0px !important; }
    	* html body { margin-top: 0px !important; }
  	</style>

  	<script type="text/javascript">
  		jQuery(document).ready(function () {
  			$('img').bind('contextmenu', function(e) {
			    return false;			    
			});
  		});  		
  	</script>
  	

</head>
<body>
	<div id="wrapper-header">
		<header id="header">
			<div id="logo">
				<a href="<?php bloginfo( 'url' ); ?>" title="<?php bloginfo( 'description' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="<?php bloginfo( 'description' ); ?>"></a>
			</div>
			<div id="right-header-top">
				<div class="distributor-image">
				<?php 
					if (eto_get_image('eto_distributor-image') <> '') {
		            	$distributorimage = eto_get_image('eto_distributor-image');
		            } else {
		                $distributorimage = get_template_directory_uri() . "/images/distributor.jpg";
		            }
		            if (eto_get_option('eto_distributor-image-link') <> '') {
		            	$distributorimagelink = eto_get_option('eto_distributor-image-link');
		            } else {
		                $distributorimagelink = "#";
		            }
				?>	
					<a href="<?php echo $distributorimagelink; ?>"><img src="<?php echo $distributorimage; ?>" alt=""></a>
				</div>				
				<?php 
					if (eto_get_option('eto_distributor-text') <> '') {
		            	$distributortext = eto_get_option('eto_distributor-text');
		            } else {
		                $distributortext = "";
		            }
		            if (eto_get_option('eto_distributor-text-link') <> '') {
		            	$distributortextlink = eto_get_option('eto_distributor-text-link');
		            } else {
		                $distributortextlink = "#";
		            }
				?>
				<div class="distributor-text">
					<a href="<?php echo $distributortextlink; ?>"><?php echo $distributortext; ?></a>
				</div>
				<div class="clear"></div>				
			</div>			
			<div id="menu-buy">
				<div id="buy-btn">
					<?php 
					if (eto_get_option('eto_buy-thermomix-url') <> '') {
		            	$buyurl = eto_get_option('eto_buy-thermomix-url');
		            } else {
		                $buyurl = "#";
		            }
		            if (eto_get_option('eto_buy-thermomix-label') <> '') {
		            	$buylabel = eto_get_option('eto_buy-thermomix-label');
		            } else {
		                $buylabel = "Buy Thermomix >";
		            }
					?>
					<a title="<?php echo $buylabel; ?>" href="<?php echo $buyurl; ?>"><?php echo $buylabel; ?></a>
				</div>
				<nav id="main-menu">
				<?php
					$defaults = array(
						'theme_location'  => 'main-menu',
						'menu'            => '',
						'container'       => 'div',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'menu',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
					wp_nav_menu( $defaults );
				?>				
			</nav>
			</div>			
			<div class="clear"></div>			
		</header>
	</div>
  <div id="wrapper-footer">
    <?php 
      if ((eto_get_option('eto_vorwerk-history') <> '') && (eto_get_image('eto_vorwerk-products') <> '')){
    ?>
    <div id="pre-footer">
      <div id="pf-left-side">
        <?php 
        if (eto_get_option('eto_vorwerk-history') <> '') {
        ?>
        <a title="<?php bloginfo( 'description' ); ?>" href="<?php bloginfo( 'url' ); ?>"><img id="img-logo-footer" src="<?php echo get_template_directory_uri(); ?>/images/logo-footer.jpg" alt="<?php bloginfo( 'description' ); ?>"></a>
        <?php 
          if (eto_get_option('eto_vorwerk-history') <> '') {
            $vorwerk_history = eto_get_option('eto_vorwerk-history');
          } else {
            $vorwerk_history = "Over more than 130 years our products have convinced millions of families thanks to their superior, innovative technology and proverbially long life.";
          }
        ?>
        <p><?php echo $vorwerk_history; ?></p>
        <?php 
        }
        ?>
      </div>
      <div id="pf-right-side">
        <?php 
        if (eto_get_image('eto_vorwerk-products') <> '') {
          $vorwerkproductsimage = eto_get_image('eto_vorwerk-products');
          ?>
          <img id="img-footer-products" src="<?php echo $vorwerkproductsimage; ?>" alt="">
          <?php
        }
        ?>        
      </div>
      <div class="clear"></div>
    </div>
    <?php 
    }
    ?>
    <footer id="footer">
      <div id="low-footer">
        <nav id="footer-left">
          <?php
          $defaults = array(
              'theme_location'  => 'footer-menu',
              'menu'            => '',
              'container'       => 'div',
              'container_class' => '',
              'container_id'    => '',
              'menu_class'      => 'menu',
              'menu_id'         => '',
              'echo'            => true,
              'fallback_cb'     => 'wp_page_menu',
              'before'          => '',
              'after'           => '',
              'link_before'     => '',
              'link_after'      => '',
              'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
              'depth'           => 0,
              'walker'          => ''
            );
            wp_nav_menu( $defaults );
          ?>
        </nav> 
        <nav id="footer-right">
          <?php 
          if (eto_get_option('eto_follow-us') <> '') {
            $followus = eto_get_option('eto_follow-us');
          } else {
            $followus = "Follow us on:";
          }
          ?>
          <span><?php echo $followus; ?></span>
          <?php 
          if (eto_get_option('eto_fb') <> '') {
            $fb = eto_get_option('eto_fb');
			?> <a title="" target="_blank" href="<?php echo $fb; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/fb.jpg" alt=""></a> <?php
          } else {
            $fb = "#";
          }
          if (eto_get_option('eto_in') <> '') {
            $in = eto_get_option('eto_in');
			?> <a title="" target="_blank" href="<?php echo $in; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/in.jpg" alt=""></a> <?php
          } else {
            $in = "#";
          }
          if (eto_get_option('eto_yt') <> '') {
            $yt = eto_get_option('eto_yt');
			?> <a title="" target="_blank" href="<?php echo $yt; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/yt.jpg" alt=""></a> <?php
          } else {
            $yt = "#";
          }
          ?> 
        </nav> 
        <div class="clear"></div>
      </div>      
    </footer>
  </div>

  <!-- Le javascript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->  

	<script type="text/javascript">
    $(".hb-img-container").mouseenter(function () {
      var mvv = $(this);
      mvv.children('div.slider-up').children('div').fadeIn(1500);
      var mvv = $(this).children('div.slider-up');
      mvv.animate({
        margin: '-240px 0 0 0'
      },
      'slow');
    });
    $(".hb-img-container").mouseleave(function () {
      var mvv = $(this);
      mvv.children('div.slider-up').children('div').fadeOut("slow");
      var mvv = $(this).children('div.slider-up');;
      mvv.animate({
        margin: '-30px 0 0 0'
      },
      'slow');
    });
    $(".hb-img-container").click(function () {
      if ($(this).attr("data-type") == "cp") {
        window.location.href = $(this).attr("data-url");
      } else {
        window.open(
          $(this).attr("data-url"),
          '_blank' 
        );
      }
      
    });
  </script>

  <style type="text/css">
    	#wpadminbar {
      		display: none;
    	}
  	</style>
  	
  	<?php wp_footer(); ?>

</body>
</html>
<?php
/*
 * Template Name: Thermomix Reprasentantin Werden page
 * Description: A Page for Thermomix Reprasentantin Werden
 */
?>

<?php
  get_header(); 
?>

<div id="wrapper-single">
	<?php if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>
	<?php 
	// Small menu
	?>
			<div id="right-header">
				<div id="wrapper-search">
					<?php include (TEMPLATEPATH . '/searchform.php'); ?>
				</div>
				<nav id="wrapper-contact">
					<?php
						$defaults = array(
							'theme_location'  => 'top-menu',
							'menu'            => '',
							'container'       => 'div',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'menu',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 0,
							'walker'          => ''
						);
						wp_nav_menu( $defaults );
					?>
				</nav>
				<div class="clear"></div>
			</div>
	<?
	// end Small menu
	?>
	<article class="buy-article">
		<?php while ( have_posts() ) : the_post(); ?>
		<h1 class="article-title"><?php the_title(); ?></h1>		
		<div class="content">
			<?php the_content(); ?>
		</div>
		<div class="rep-form">			
			<?php echo do_shortcode( eto_get_option('eto_rep-code') ); ?>
		</div>
		<div class="clear"></div>
		<?php endwhile; // end of the loop. ?>
	</article>
	<div class="clear"></div>
</div>

<?php 
	get_footer(); 
?>
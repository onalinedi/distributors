<?php
  get_header(); 
?>

<div id="wrapper-single">
	<?php if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	} ?>
	<?php 
	// Small menu
	?>
			<div id="right-header">
				<div id="wrapper-search">
					<?php include (TEMPLATEPATH . '/searchform.php'); ?>
				</div>
				<nav id="wrapper-contact">
					<?php
						$defaults = array(
							'theme_location'  => 'top-menu',
							'menu'            => '',
							'container'       => 'div',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'menu',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 0,
							'walker'          => ''
						);
						wp_nav_menu( $defaults );
					?>
				</nav>
				<div class="clear"></div>
			</div>
	<?
	// end Small menu
	?>
	<?php get_sidebar('left'); ?>
	<?php 
		if (is_active_sidebar( 'left-sidebar' )) {
			$articleclass = "sidebar-yes";
		} else {
			$articleclass = "sidebar-no";
		}
	?>	
	<article class="<?php echo $articleclass; ?>">		
		<h1 class="article-title"><span>Error: 404</span></h1>		
		<div class="content">
			Page not Found
		</div>
		<div class="content-image">
			
		</div>
		<div class="clear"></div>
	</article>
	<div class="clear"></div>
</div>

<?php 
	get_footer(); 
?>
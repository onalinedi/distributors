<?php 

/*******************************************************************
	Site Title
*******************************************************************/
function allthemes_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'thermomix' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'allthemes_wp_title', 10, 2 );
/*******************************************************************
	End Title
*******************************************************************/

/*
	Adding custom jQuery
*/
add_action("wp_enqueue_scripts", "my_enqueue");
function my_enqueue() {
   	wp_deregister_script('jquery');
   	wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.js', false, null);
   	wp_enqueue_script('jquery'); 
}

/*
	Adding post thumbnails
*/
add_theme_support( 'post-thumbnails', array( 'post'));

/*
	Adding menu support
*/
add_theme_support( 'menus' );
register_nav_menu('top-menu', 'Top menu');
register_nav_menu('main-menu', 'Main menu');
register_nav_menu('sidebar-menu', 'Sidebar menu');
register_nav_menu('footer-menu', 'Footer menu');

/*
	Adding Home Block Content Type
*/
add_action('init', 'Home_block', 0);
function Home_block() {
	$args = array(
			'description' => 'Home Block Post Type',
			'show_ui' => true,
			'menu_position' => 4,
			'labels' => array(
				'name'=> 'Home Blocks',
				'singular_name' => 'Home Block',
				'add_new' => 'Add New Home Block',
				'add_new_item' => 'Add New Home Block',
				'edit' => 'Edit Home Block',
				'edit_item' => 'Edit Home Block',
				'new-item' => 'New Home Block',
				'view' => 'View Home Block',
				'view_item' => 'View Home Block',
				'search_items' => 'Search Home Blocks',
				'not_found' => 'No Home Blocks Found',
				'not_found_in_trash' => 'No Home Blocks Found in Trash',
				'parent' => 'Parent Listing'
			),
			'public' => true,
			//'taxonomies' => array('manufacturer'),
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'home-block', 'with_front' => false ),
			'supports' => array('title', 'editor', 'thumbnail')
	);
	register_post_type( 'home-block' , $args );
}

/*
	Adding Right Sidebar
*/
if ( function_exists('register_sidebar') ) {
	register_sidebar(array('name'=>'Left sidebar',
			'id' => "left-sidebar",
			'before_widget' => "<div class='widget'>",
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3 >',
	));
}

/*
	Adding formats to tinymce
*/


/** 
 * Add "Styles" drop-down 
 */   
add_filter( 'mce_buttons_2', 'thermomix_mce_editor_buttons' );  
function thermomix_mce_editor_buttons( $buttons ) {  
    array_unshift( $buttons, 'styleselect' );  
    return $buttons;  
} 
/** 
 * Add styles/classes to the "Styles" drop-down 
 */   
add_filter( 'tiny_mce_before_init', 'thermomix_mce_before_init' );   
function thermomix_mce_before_init( $settings ) {  
  
    $style_formats = array(  
        array(  
            'title' => 'Intro-Text',  
            'inline' => 'span',  
            'styles' => array(  
			                'color' => '#113c2b',  
			                'fontWeight' => 'regular',  
			                'fontSize' => '20px',
			                'fontFamily' => 'Arial'  
        )),  
        array(  
		    'title' => 'Copy-Text',  
		    'inline' => 'span',  
		    'styles' => array(  
		                	'color' => '#113c2b',    
		                	'fontSize' => '13px',
		                	'fontFamily' => 'Arial'  
		)),
        array(  
		    'title' => 'Headline H2',  
		    'block' => 'h2',  
		    'styles' => array(  
		                	'color' => '#113c2b',    
		                	'fontSize' => '18px',
		                	'fontWeight' => 'bold',
		                	'fontFamily' => 'Arial'  
		)),
		array(  
		    'title' => 'Headline H2 whit enphasis',  
		    'block' => 'h2',  
		    'styles' => array(  
		                	'color' => '#009a3d',    
		                	'fontSize' => '18px',
		                	'fontWeight' => 'bold',
		                	'fontFamily' => 'Arial'  
		)),
        array(  
		    'title' => 'Headline H3', 
		    'inline' => 'h3',  
		    'styles' => array(  
		                	'color' => '#113c2b',    
		                	'fontSize' => '16px',
		                	'fontWeight' => 'bold',
		                	'fontFamily' => 'Arial'  
		)),
		array(  
		    'title' => 'Headline H4', 
		    'inline' => 'h4',  
		    'styles' => array(  
		                	'color' => '#113c2b',    
		                	'fontSize' => '13px',
		                	'fontWeight' => 'bold',
		                	'fontFamily' => 'Arial'  
		)),
		array(  
		    'title' => 'Headline H5', 
		    'inline' => 'h5',  
		    'styles' => array(  
		                	'color' => '#113c2b',    
		                	'fontSize' => '13px',
		                	'fontWeight' => 'bold',
		                	'fontFamily' => 'Arial'  
		)),
		array(  
		    'title' => 'Headline H6', 
		    'inline' => 'h6',  
		    'styles' => array(  
		                	'color' => '#113c2b',    
		                	'fontSize' => '13px',
		                	'fontWeight' => 'bold',
		                	'fontFamily' => 'Arial'  
		))
    	);  
  
    $settings['style_formats'] = json_encode( $style_formats );  
  
    return $settings;  
  
}  

/** 
 * Adding thermovideo shortcode 
 */  
function thermovideo_func( $atts ) {
	extract( shortcode_atts( array(
		'id' => ''
	), $atts ) );

	return "<div class='thermovideobox' style='margin: 4px 10px; float: left;'><a target='_blank' href='http://www.youtube.com/watch?v={$id}&feature=youtube_gdata_player'><img width='290px' src='http://i1.ytimg.com/vi/{$id}/0.jpg'/></a></div>";
}
add_shortcode( 'thermovideo', 'thermovideo_func' );
// [thermovideo id='_SQkWbRublY']
